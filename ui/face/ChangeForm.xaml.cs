﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace face
{
    /// <summary>
    /// Логика взаимодействия для CreateForm.xaml
    /// </summary>
    public partial class ChangeForm : Window {
        public Guid Id;
        private string _script;
        public ChangeForm(Guid id) {
            InitializeComponent();
            Id = id;
            _script = "";
            Lbl_Guid.Content = Id.ToString();
        }

        public ChangeForm(Guid id, string description, string script, string operation) {
            InitializeComponent();
            Id = id;
            _script = script ?? "";
            Lbl_Guid.Content = Id.ToString();
            TB_Description.Text = description;
            TB_Operation.Text = operation;
        }

        private void Btn_Create_Click(object sender, RoutedEventArgs e) {
            G.InsertReplaceGanglion(Id, TB_Description.Text, _script, TB_Operation.Text);
            this.Close();
        }

        private void Btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TB_Operation_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
