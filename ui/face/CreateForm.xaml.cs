﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace face
{
    /// <summary>
    /// Логика взаимодействия для CreateForm.xaml
    /// </summary>
    public partial class CreateForm : Window {
        public Guid Id;
        public CreateForm() {
            InitializeComponent();

            Id = Guid.NewGuid();
            Lbl_Guid.Content = Id.ToString();
        }

        private void Btn_Create_Click(object sender, RoutedEventArgs e) {
            G.InsertReplaceGanglion(Id, TB_Description.Text, "", TB_Operation.Text);
            this.Close();
        }

        private void Btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
