﻿using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;

namespace face {
    /// <summary>
    /// Логика взаимодействия для BindingForm.xaml
    /// </summary>
    public partial class BindingForm : Window {
        private List<string> _inEventsAfterEditing;
        private Ganglion _ganglion;
        public int CurrentIndex;

        public BindingForm(Ganglion ganglion) {
            InitializeComponent();
            _ganglion = ganglion;
            _inEventsAfterEditing = new List<string>();
            foreach (var evt in _ganglion.InEvents) {
                _inEventsAfterEditing.Add(evt);
            }
            LB_Binding.ItemsSource = _ganglion.InEvents;
            CurrentIndex = LB_Binding.SelectedIndex;

        }

        private void Btn_Save_Click(object sender, RoutedEventArgs e) {
            _ganglion.SaveInEvents();
            this.Close();
        }

        private void Btn_Cancel_Click(object sender, RoutedEventArgs e) {
            _ganglion.InEvents.Clear();
            foreach (var evt in _inEventsAfterEditing) {
                _ganglion.InEvents.Add(evt);
            }
            this.Close();
        }

        private void Btn_Add_Click(object sender, RoutedEventArgs e) {
            if (TB_NewEvent.Text != "") {
                _ganglion.InEvents.Add(TB_NewEvent.Text);
                TB_NewEvent.Text = "";
            } else {
                MessageBox.Show("Пожалуйста введите название события");
            }
            
            LB_Binding.Items.Refresh();
        }

        private void Btn_Rem_Click(object sender, RoutedEventArgs e) {
            if (CIndexIsValid()) {
                _ganglion.InEvents.RemoveAt(CurrentIndex);
                LB_Binding.Items.Refresh();
            }
        }

        private void LB_Binding_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            CurrentIndex = LB_Binding.SelectedIndex;
        }

        private bool CIndexIsValid() {
            return (CurrentIndex >= 0) && (CurrentIndex < _ganglion.InEvents.Count);
        }
    }
}