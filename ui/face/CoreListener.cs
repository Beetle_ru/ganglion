﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConnectionProvider;
using CommonTypes;
//using Implements;
using FlexEventHome;

namespace face {
    internal class CoreListener : IEventListener {


        public CoreListener() {
            Console.WriteLine("Listener started");
        }

        public void OnEvent(BaseEvent evt) {
            if (evt is FlexEvent) {
                var fex = evt as FlexEvent;
                if ((fex.Flags & FlexEventFlag.FlexEventOpcNotification) != 0) {
                    foreach (var ganglion in G.GanlionList) {
                        ganglion.CheckAndSetValues(fex);
                    }
                }
                //Console.WriteLine(fex);
            }
        }

    }
}