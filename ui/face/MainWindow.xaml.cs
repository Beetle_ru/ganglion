﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace face
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private int _currentIndex;
        private bool _isDeleteProcess;

        public MainWindow() {
            InitializeComponent();
            G.LoadGL();
            DG_GanglionView.ItemsSource = G.GanlionList;

            _currentIndex = DG_GanglionView.SelectedIndex;
            _isDeleteProcess = false;
        }

        private void DG_GanglionView_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (CIndexIsValid() && (TB_Script.Text != G.GanlionList[_currentIndex].Script)) {
                var result = MessageBox.Show(
                    "Данные были изменены, сохранить изменение?", 
                    "Внимание!", 
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning
                );

                if (result == MessageBoxResult.Yes) {
                    Btn_ScriptApply_Click(new object(), new RoutedEventArgs());
                }
            }

            _currentIndex = DG_GanglionView.SelectedIndex;
            if (CIndexIsValid()) {
                TB_Script.Text = G.GanlionList[_currentIndex].Script;
            }
        }

        private void Btn_ScriptApply_Click(object sender, RoutedEventArgs e) {
            if (CIndexIsValid()) {
                var ganglion = G.GanlionList[_currentIndex];
                ganglion.Script = TB_Script.Text;
                G.InsertReplaceGanglion(ganglion.Id, ganglion.Description, ganglion.Script, ganglion.OutOperation);
            }
        }

        private void Btn_CreateGanglian_Click(object sender, RoutedEventArgs e) {
            var createForm = new ChangeForm(Guid.NewGuid());
            createForm.ShowDialog();
            DG_GanglionView.Items.Refresh();
        }

        private void Btn_DeleteGanglian_Click(object sender, RoutedEventArgs e) {
            if (CIndexIsValid()) {
                _isDeleteProcess = true;
                G.DeleteGanglion(G.GanlionList[_currentIndex].Id);
                _currentIndex = DG_GanglionView.SelectedIndex = -1; 
                DG_GanglionView.Items.Refresh();
                _isDeleteProcess = false;
            } else {
                MessageBox.Show("Пожалуйста выберете пункт для удаления!");
            }
        }

        private bool CIndexIsValid() {
            return (_currentIndex >= 0) && (_currentIndex < G.GanlionList.Count) && (!_isDeleteProcess);
        }

        private void Btn_Execute_Click(object sender, RoutedEventArgs e) {
            if (CIndexIsValid()) {
                G.GanlionList[_currentIndex].Execute();
            }
            else {
                MessageBox.Show("Пожалуйста выберете пункт для исполнения!");
            }
        }

        private void Btn_Path_Click(object sender, RoutedEventArgs e) {
            if (CIndexIsValid()) {
                var createForm = new BindingForm(G.GanlionList[_currentIndex]);
                createForm.ShowDialog();
            }
            else {
                MessageBox.Show("Пожалуйста выберете пункт для привязки!");
            }
        }

        private void Btn_Settings_Click(object sender, RoutedEventArgs e) {
            if (CIndexIsValid()) {
                var createForm = new ChangeForm(G.GanlionList[_currentIndex].Id, G.GanlionList[_currentIndex].Description, G.GanlionList[_currentIndex].Script, G.GanlionList[_currentIndex].OutOperation);
                
                createForm.ShowDialog();
                DG_GanglionView.Items.Refresh();
            }
            else {
                MessageBox.Show("Пожалуйста выберете пункт для настройки!");
            }
        }

    }
}
