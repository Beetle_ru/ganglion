﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using ConnectionProvider;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;

namespace face {
    public class Ganglion {
        private readonly Dictionary<string, object> StaticVars;
        private readonly Dictionary<string, object> InVars;
        public Guid Id { set; get; }
        public string Description { set; get; }
        public string OutOperation { set; get; }
        public List<string> InEvents;
        public string Script;

        public Ganglion(Guid id, string description, string script, string outOperation) {
            Id = id;
            Description = description;
            Script = script;
            OutOperation = outOperation;

            InEvents = new List<string>();
            StaticVars = new Dictionary<string, object>();
            InVars = new Dictionary<string, object>();

            InVars.Add("$.Id", Id);
            InVars.Add("$.Description", Description);
            InVars.Add("$.OutOperation", OutOperation);
            InVars.Add("$.InEvents", InEvents);
            InVars.Add("$.Script", Script);
        }

        

        public void Execute() {
            LoadSV();
            var res = ScriptRun();
            FireResult(res);
            SaveSV();
        }

        private Dictionary<string, object> ScriptRun()
        {
            ScriptEngine engine = Python.CreateEngine();
            ScriptScope scope = engine.CreateScope();
            ScriptSource source = engine.CreateScriptSourceFromString(Script);
            CompiledCode compiled;
            var outVars = new Dictionary<string, object>();

            try {
                compiled = source.Compile(); // для ускорения выполнения компиляцию вынести в другое место
                scope.SetVariable("GLOBAL", StaticVars);
                scope.SetVariable("IN", InVars);
                scope.SetVariable("OUT", outVars);
                compiled.Execute(scope);
            }
            catch (Exception e) {
                Console.WriteLine("id = {0}, Description = {1} Execute error:", Id, Description);
                Console.WriteLine(e.Message);
            }

            return outVars;
            /*foreach (var outVar in OutVars) {
                Console.WriteLine("k = {0}, v = {1}, t = {2}", outVar.Key, outVar.Value, outVar.Value.GetType());
            }*/
        }

        public void CheckAndSetValues(FlexEventHome.FlexEvent fex) {
            var isFound = false;
            foreach (var inEvent in InEvents) {
                if (inEvent == fex.Operation) {
                    isFound = true;
                    foreach (var argument in fex.Arguments) {
                        var varName = String.Format("{0}.{1}", fex.Operation, argument.Key);
                        if (InVars.ContainsKey(varName)) {
                            InVars[varName] = argument.Value;
                        } else {
                            InVars.Add(varName, argument.Value);
                        }
                    }
                }
            }
            if (isFound) Execute();
        }

        private void LoadSV() {
            //Console.WriteLine("LOAD STATIC VARS:");
            lock (G.DBLocker) {
                using (var conn = new SQLiteConnection("Data Source=" + G.DBName)) {
                    conn.Open();
                    using (SQLiteCommand c = conn.CreateCommand()) {
                        c.CommandText = String.Format("select name, value from SVars where gid = '{0}'", Id);
                        try {
                            using (SQLiteDataReader reader = c.ExecuteReader()) {
                                while (reader.Read()) {
                                    string name = null;
                                    string value = null;

                                    for (int i = 0; i < reader.FieldCount; i++) {
                                        //Console.Write(reader[i] + " | ");
                                        string header = reader.GetName(i);

                                        if (header == "name") {
                                            name = (string) reader[i];
                                        }
                                        else if (header == "value") {
                                            value = (string) reader[i];
                                        }
                                    }
                                    if ((name != null) && (!StaticVars.ContainsKey(name))) {
                                        StaticVars.Add(name, value);
                                    }
                                    else {
                                        //Console.Write(" **** ");
                                    }
                                    //Console.WriteLine();
                                }
                            }
                        }
                        catch (Exception e) {
                            Console.WriteLine(e);
                        }
                    }
                    conn.Close();
                }
            }
        }

        private void SaveSV() {
            foreach (var sv in StaticVars) {
                string cmd =
                    String.Format("insert or replace into SVars (name, value, gid) values ('{0}', '{1}', '{2}')",
                                  sv.Key,
                                  sv.Value,
                                  Id
                        );
                G.SQLNonQuery(cmd);
            }
        }

        private void FireResult(Dictionary<string, object> result) {
            if ((result.Count <= 0) || (OutOperation.Trim() == "")) return;
            var fex = new FlexHelper(OutOperation);
            foreach (var o in result) {
                fex.AddArg(o.Key,o.Value);
            }
            fex.Fire(G.MainGate);
        }

        public void LoadInEvents() {
            InEvents = new List<string>();
            //Console.WriteLine("LOAD INEVENTS:");
            lock (G.DBLocker) {
                using (var conn = new SQLiteConnection("Data Source=" + G.DBName)) {
                    conn.Open();
                    using (SQLiteCommand c = conn.CreateCommand()) {
                        c.CommandText = String.Format("select operation from InEvents where gid = '{0}'", Id);
                        try {
                            using (SQLiteDataReader reader = c.ExecuteReader()) {
                                while (reader.Read()) {
                                    for (int i = 0; i < reader.FieldCount; i++) {
                                        //Console.Write(reader[i] + " | ");
                                        string header = reader.GetName(i);

                                        if (header == "operation") {
                                            InEvents.Add((string) reader[i]);
                                        }
                                    }
                                    //Console.WriteLine();
                                }
                            }
                        }
                        catch (Exception e) {
                            Console.WriteLine(e);
                        }
                    }
                    conn.Close();
                }
            }
        }

        public void SaveInEvents() {
            G.SQLNonQuery(String.Format("delete from InEvents where gid = '{0}'", Id));
            foreach (var ie in InEvents)
            {
                string cmd =
                    String.Format("insert or replace into InEvents (operation, gid) values ('{0}', '{1}')",
                                  ie,
                                  Id
                        );
                G.SQLNonQuery(cmd);
            }
        }
    }
}