﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.IO;
using ConnectionProvider;
using FlexEventHome;

namespace face {
    static class G {
        public static List<Ganglion> GanlionList = new List<Ganglion>();
        public static string DBName = "store.db";
        public static ConnectionProvider.Client MainGate;
        public static object DBLocker = new object();

        static G() {
            var o = new FlexEvent();
            MainGate = new ConnectionProvider.Client(new CoreListener());
            MainGate.Subscribe();
        }

        public static void LoadGL() {
            if (!TestDB()) InitDB();

            lock (DBLocker) {
                using (var conn = new SQLiteConnection("Data Source=" + DBName)) {
                    conn.Open();
                    using (var c = conn.CreateCommand()) {
                        c.CommandText = "select * from Ganglion";
                        using (var reader = c.ExecuteReader()) {
                            while (reader.Read()) {
                                var id = new Guid();
                                string description = null;
                                string script = null;
                                string operation = null;

                                for (int i = 0; i < reader.FieldCount; i++) {
                                    //Console.Write(reader[i] + " | ");
                                    var header = reader.GetName(i);

                                    if (header == "id") {
                                        id = Guid.Parse((string) reader[i]);
                                    }
                                    else if (header == "name") {
                                        description = (string) reader[i];
                                    }
                                    else if (header == "script") {
                                        script = (string) reader[i];
                                    }
                                    else if (header == "operation") {
                                        operation = (string) reader[i];
                                    }
                                }
                                //Console.WriteLine();
                                GanlionList.Add(new Ganglion(id, description, script, operation));
                            }
                        }
                    }
                    conn.Close();
                }
            }
            foreach (var ganglion in GanlionList) {
                ganglion.LoadInEvents();
            }
        }

        public static void SaveGL() {
            foreach (var ganglion in GanlionList) {
                var cmd = String.Format("insert or replace into Ganglion (id, name, script, operation) values ('{0}', '{1}', '{2}', '{3}')",
                    ganglion.Id,
                    ganglion.Description,
                    ganglion.Script,
                    ganglion.OutOperation
                );
                SQLNonQuery(cmd);
            }
        }

        public static void InsertReplaceGanglion(Guid id, string description, string script, string outOperation) {
            var found = false;
            foreach (var ganglion in GanlionList) {
                if (ganglion.Id == id) {
                    found = true;
                    ganglion.Description = description;
                    ganglion.Script = script;
                    ganglion.OutOperation = outOperation;
                    break;
                }
            }

            if (!found) GanlionList.Add(new Ganglion(id, description, script, outOperation));

            var cmd = String.Format("insert or replace into Ganglion (id, name, script, operation) values ('{0}', '{1}', '{2}', '{3}')",
                id,
                description,
                script,
                outOperation
            );

            SQLNonQuery(cmd);
        }

        public static void DeleteGanglion(Guid id) {
            for (int i = 0; i < GanlionList.Count; i++) {
                if (GanlionList[i].Id == id) {
                    GanlionList.RemoveAt(i);
                    var commands = new[] {
                        String.Format("delete from Ganglion where id = '{0}'", id),
                        String.Format("delete from SVars where gid = '{0}'", id),
                        String.Format("delete from InEvents where gid = '{0}'", id)
                    };

                    foreach (var cmd in commands) {
                        SQLNonQuery(cmd);
                    }
                    break;
                }
            }
        }

        public static void InitDB() {
            SQLiteConnection.CreateFile(DBName);
            var commands = new[] {
                "create table Ganglion (id primary key, name, script, operation)",
                "create table SVars (name primary key, value, gid)",
                "create table InEvents (operation primary key, gid)",
               /* "insert into Ganglion (id, name, script, operation) values ('" + Guid.NewGuid() + "', 'tetetest', 'script script', 'flexName')",
                "insert into Ganglion (id, name, script, operation) values ('" + Guid.NewGuid() + "', 'tetetest', 'script script', 'flexName')",
                "insert into Ganglion (id, name, script, operation) values ('" +  new Guid() + "', 'tetetest', 'script script', 'flexName')",
                "insert or replace into Ganglion (id, name, script, operation) values ('" +  new Guid() + "', 'zzz', 'script script', 'flexName') ",*/

               /*"insert into Ganglion (id, name, script, operation) values ('" +  new Guid() + "', 'tetetest', 'script script', 'flexName')",
               "insert or replace into SVars (name, value, gid) values ('v1', '10', '" +  new Guid() + "')",
               "insert or replace into SVars (name, value, gid) values ('v2', '20', '" +  new Guid() + "')",
               "insert or replace into SVars (name, value, gid) values ('v3', '30', '" +  new Guid() + "')",
               "insert or replace into SVars (name, value, gid) values ('v1', 'suka', '" +  new Guid() + "')"*/
            };

            foreach (var cmd in commands) {
                SQLNonQuery(cmd);
            }

        }

        public static bool TestDB() {
            return File.Exists(DBName);
        }

        public static void SQLNonQuery(string cmd) {
            lock (DBLocker) {
                using (var conn = new SQLiteConnection("Data Source=" + DBName)) {
                    conn.Open();
                    using (var c = conn.CreateCommand()) {
                        c.CommandText = cmd;
                        try {
                            c.ExecuteNonQuery();
                        }
                        catch (Exception e) {
                            Console.WriteLine(e);
                        }
                    }
                    conn.Close();
                }
            }
        }
    }
}
