﻿using System;

namespace FlexEventHome
{
    [Serializable]
    public class SteelAttributes
    {
        public int Temperature { set; get; }
        public double Carbon { set; get; }
    }
}