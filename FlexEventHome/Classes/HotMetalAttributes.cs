﻿using System;

namespace FlexEventHome
{
    [Serializable]
    public class HotMetalAttributes
    {
       public int Temperature { get ; set;  }
       public int Weight { get ;set ; }
    }
}
