﻿using System;

namespace FlexEventHome
{
    [Serializable]
    public class Sublance
    {
        public DateTime StartDate { get; set; }
        public int Temperature { get; set; }
        public int Oxigen { set; get; }
        public double C { set; get; }
    }
}
