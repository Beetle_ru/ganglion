﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace FlexEventHome.API
{
    [ServiceContract(Namespace = "http://Converter.API", SessionMode = SessionMode.Required)]
    public interface IConverterAPI
    {
        [OperationContract()]
        Int64 GetHeatNumber();

        [OperationContract()]
        int GetConverterNumber();

        [OperationContract()]
        int GetTeamNumber();

        [OperationContract()]
        string GetGrade();

        [OperationContract()]
        SteelAttributes GetActualSteelAttributes();

        [OperationContract()]
        SteelAttributes GetPlannedSteelAttributes();

        [OperationContract()]
        DateTime GetBlowingStartTime();

        [OperationContract()]
        List<string> GetBlowingSchemas();

    }
}