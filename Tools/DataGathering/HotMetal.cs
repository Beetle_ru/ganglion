﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Converter;


namespace DataGathering
{

    class HotMetal : HotMetalAttributes
    {
        public float C { get; set; }
        public float Si { get; set; }
        public float Mn { get; set; }
        public float P { get; set; }
        public float S { get; set; }
        public float Ti { get; set; }
    }
}
